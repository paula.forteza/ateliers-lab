# Atelier Publier ses frais de mandat en open data

## TODO

* Comment et où générer les clés ssh ?
  * Installer un serveur avec weboob
  * Créer des comptes utilisateurs
  * Autre solution, utiliser la CI pour ajouter les commandes
  * Peut-on utiliser la même clé SSH partout ?
    NON : risque de confusion entre les différents projets
- Passer houx en HTTPS
- Supprimer les jobs quand le dernier se finit bien

## Done

* Importer La Poste
* Faire une image Docker parlement-ouvert/weboob
  * Non FramaGit n'accepte que quelques types d'images
* 2FA sur GitLab
  * Non trop complexe


## Besoins

### Députés

* Avant l'atelier, déclarer sa banque utilisée
  * => Faire un formulaire ou un pad pour les déclarer
* Venir avec son identifiant et son mot de passe de la banque

### Services de l'Assemblée

L'idéal serait que les services de l'assemblée rajoutent les liens vers les hledger-web pendant la formation.

En parler avec Vannina ? Eric Buge ?

L'idéal serait que ceux qui pratiquent l'open data n'aient pas à subir le contrôle aléatoire ou que ce soit moins fréquent

Leur proposer d'héberger à terme le serveur.

### Moi

Un seul serveur ou un serveur par député ?

Un serveur :
* L'assemblée peut reprendre
* Premier pas vers un chaton
* Permet d'itérer et de mettre à jour les formats de données simplement (sur les catégories des données personnels)

* Faire une UI pour permettre aux députés de saisir leur login et leur mot de passe et double facteur pour mettre à jour leurs données ???
* Ou stocker les identifiants et les mots de passe ?
  * Demander
* Reporter le pb sur les banques qui ne proposent pas d'accès en lecture seule à leur compte

* Ajouter le 2FA ???

* Utiliser Maxime2 pour faire un audit de sécurité

### Regards citoyens

Leur demander :
* s'ils peuvent moissonner et mettre dans leurs stats ?
* Quels seraient les indicateurs, mots clés nécessaires selon eux ?

A faire peut-être dans un second temps, par exemple dans un hackathon ou autre

## Architecture

* Dépôt GitLab des fichiers CSV (un dépôt par député)
  * Contient un fichier .gitlab-ci.yml qui récupère le CSV de la banque avec Weboob et qui l'ajoute dans un fichier YYYY/MM/DD.csv du dépôt des CSV et qui commite
    * Le job contient l'option `when: manual` afin de ne pas être exécuté à chaque push

  * Ce fichier CI contient le login et le password bancaire qui sont soit des variables privées GitLab soit fournies en paramètre par un POST qui provoque l'exécution du CI
  * Ce fichier CI utilise une image Docker Weboob
  * Si le login et le password ont été mis en variable secrète on peut mettre dans les schedules GitLab du projet un lancement quotidien (ou autre) du CI pour avoir l'IRFM mise à jour en permanence
  * Trigger qui lance l'exécution du .gitlab-ci.yml au moment du push le lance la fusion des fichiers CSV en un seul (en enlevant les doublons), ainsi que la conversion de ce fichier
* Dépot GitLab du script de fusion des CSV (mutualisé)
  * Son .gitlab-ci.yml est lancé par les webhook précédents
    * pull du dépôt des CSV du député (nom fourni en variable par le webhook)
    * pull du dépôt contenant le fichier CSV fusionné et le fichier comptable Beancount
    * fusion du CSV pour générer un fichier comptable Beancount
      * Pour fusionner une ligne on prendre le fichier le plus récent et on prend ses lignes une par une :
        * Si la ligne est déjà présente on la saute
    * commit et push du dépôt contenant le fichier CSV fusionné et le fichier comptable
* Dépot du fichier CSV fusionné et du fichier comptable Beancount (un par député)
  * Son .gitlab-ci.yml est exécuté par un push
    * génération et publication d'un site web statique GitLab Pages à partir du fichier comptable, en utilisant bean-bake

* Application web (mutualisée)
  * Application server-less (=> Gitlab Pages) qui demande l'URL du dépot GitLab de récupération des comptes bancaires (ou plutôt le slug du député), ainsi que le login et le password du compte bancaire, et qui lance un trigger sur le dépôt GitLab de récupération bancaire en POSTant le login et le password.
  * Une fois le POST fait a minima, elle affiche les URL où on pourra voir les données et le site web et propose de revenir à la page du formulaire

### Questions

* Utilise-t-on FramaGit ou un nouveau GitLab git.parlement-ouvert.fr ?

* Page web optionnelle qui trigger un GitLab-CI
*
* Beancount, hledger ou ledger ?
* Comment fusionner les fichiers CSV ?
* Un serveur par député ou on mutualise ?
* On stocke le fichier CSV brut dans Git.
* Puis intégration continue pour générer des pages web statiques avec bean-bake
* UI web avec une page demandant :
  - l'email du député : permet de retrouver sa configuration (nom de la banque, nom de sa page web hledger)
  - l'identifiant bancaire
  - mot de passe bancaire du député
  - la page web appelle le script d'intégration continue en lui fournissant en variable le nom et mot de passe bancaire
    - Les 2 variables peuvent aussi être mises en variable secrète de GitLab ce qui permet une mise à jour quotidienne automatique
      - Mais c'est un choix du député
    -
* Un hledger-web par compta et donc par député
  * Chaque député doit venir avec son compte pour pouvoir ajouter le site à son DNS
  * Un fichier Apache ou NGINX à configurer par député et il faut donner le nom de domaine

Les informations comme le compte bancaire sont chiffrées avec l'OTP et le mot de passe

## Objectifs

Préparer les députés aux technos Git et GitLab d'Amenda

### Comptabilité en mode texte

* Intérêt
  * Permet d'utiliser les outils des développeurs
    * Historique

### Git

* Intérêt
  * outils des développeurs

### Intégration continue

### Ouverture de bout en bout

* Transparence des process

## Plan

---

# Présentation d'un dépôt Git

TODO : Enlever les tickets, etc

---

# Création d'un projet de test privé

```yaml
image: python:3

generate_ssh_key:
  script:
    - ssh-keygen -b 4096 -C "bot" -f bot_id_rsa -t rsa
    - cat bot_id_rsa
    - cat bot_id_rsa.pub
```

Recopier le contenu afficher dans 2 fichiers `bot_id_rsa` et `bot_id_rsa.pub`

### Génération d'une clé SSH

### L'état des demandes

Vote de la loi

Demande de RC d'avoir les données

### Présentation de la comptabilité en mode texte

### Enregistrement auprès des services de l'Assemblée

### Annonce de la disponibilité sur les réseaux sociaux

TODO: Faire des exemples de messages pour les réseaux sociaux

### Contribution à la traduction de hledger web

