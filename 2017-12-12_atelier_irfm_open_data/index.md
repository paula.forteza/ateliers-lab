class: center, middle

# Lab : Publier ses frais de mandat en open data

## Atelier du 12 décembre 2017


_La présentation de cet atelier est disponible en ligne sur :_
.center[http://paula.forteza.frama.io/ateliers-lab/]

---
class: center, middle

# Présentation du ![Logo du Lab](images/logo_lab.jpeg) La REM

## Pourquoi cet atelier ?

---

# Création d'un compte sur FramaGit

* Aller sur Framagit : https://framagit.org
* Cliquer sur le bouton `Sign in / Register`
* Cliquer sur l'onglet `Register`
* Ajouter le courriel du député et le `Username` Framagit dans le pad :
  https://hackmd.io/AwMwhgzATAnAjDAtAEzGxAWAbMgrIgDgFM4JEwYxh4oEQDcg?edit

---

# Création du projet contenant les relevés bancaires

* Cliquer sur le bouton `+` dans le menu du haut à droite
* Cliquer sur le menu `New project`

.center[![Framagit — Nouveau projet pour les relevés bancaires](images/framagit_new_irfm_data_project.png)]

* Nommer le projet `irfm-data`
* S'assurer que le projet est public (c'est de l'open data)
* Cliquer sur le bouton `Create project`

---

# Configuration du projet 1/2

* Cliquer sur le menu `Settings` en bas à gauche
* Cliquer sur le sous-menu `General`
* Dans la zone `Permissions`, cliquer sur le bouton `Expand`
* Cliquer sur les boutons ci-après pour les désactiver :

---

# Configuration du projet 2/2

.center[![Framagit — Permissions](images/framagit_permissions.png)]

Puis cliquer sur le bouton `Save changes`

---

# Création d'un projet pour chaque député

Pendant ce temps, à partir du pad, le formateur doit créer le projet `irfm-prenom.nom-script` pour chaque député et ajouter les comptes utilisateurs correspondant en _Owner_.

Une fois le projet de votre député créé et rattaché à votre compte :

* Cliquer sur le bouton de menu `Projects` en haut à gauche
* Cliquer sur le menu `Your projects`
* La liste de vos projets apparaît
* Cliquer sur le projet `irfm-prenom.nom-script`

---

# Configuration du projet

* Cliquer sur le menu `Settings` en bas à gauche
* Cliquer sur le sous-menu `General`
* Dans la zone `Permissions`, cliquer sur le bouton `Expand`
* Cliquer sur les boutons ci-dessous pour les désactiver :

.center[![Framagit — Permissions](images/framagit_permissions.png)]

Puis cliquer sur le bouton `Save changes`

---

# Génération d'une bi-clé SSH

SSH fonctionne avec 2 clés :
* 1 clé privée
  * protégée par un mot de passe
  * doit rester secrète
  * ne **jamais** la communiquer à un tiers
* 1 clé publique
  * à mettre sur chaque serveur où vous voulez vous connecter par SSH
  * peut être diffusée

---

# Mécanisme de clé privée et clé publique

Les fonctions à sens unique sont des fonctions mathématiques telles qu'une fois appliquées à un message, il est extrêmement difficile de retrouver le message original.

L'existence d'une brèche secrète permet cependant à la personne qui a conçu la fonction à sens unique de décoder facilement le message grâce à un élément d'information qu'elle possède, appelé clef privée.

Exemple :
* 2 nombres premiers = clé privée
* produit de ces 2 nombres = clé publique
  * = nombre quasi-premier
  * difficile de retrouver les 2 nombres premiers
    * quand les nombres premiers sont très grands

---

# Création du premier script 1/3

Nous allons écrire un script dans notre projet pour générer une clé SSH.

* Cliquer sur le menu `Overview` en haut à gauche
* Cliquer sur le bouton de menu `+`
* Cliquer sur le menu `New file`

.center[![Framagit — Menu de nouveau fichier](images/framagit_new_file_menu.png)]

---

# Création du premier script 2/3

* Dans le champ `File name`, taper : `.gitlab-ci.yml`
* Dans la zone d'édition, taper :
  ```yaml
  image: python:3

  generate_ssh_key:
    script:
      - ssh-keygen -b 4096 -C "bot" -f bot_id_rsa -t rsa
      - cat bot_id_rsa
      - cat bot_id_rsa.pub
  ```
* Cliquer sur le bouton `Commit changes`

---

# Création du premier script 3/3

Vérifier que le fichier de configuration GitLab CI est valide :

.center[![Framagit — Fichier de configuration GitLab CI valide](images/framagit_valid_gitlab_ci.png)]

---

# Le rôle particulier du fichier `.gitlab-ci.yml`

Le fichier `.gitlab-ci.yml` est le fichier qui permet l'intégration continue (CI) et le déploiement continue (CD).

Ce fichier est exécuté à chaque fois que le dépôt qui le contient est modifié.

Or en créant le fichier `.gitlab-ci.yml` on vient de l'ajouter au dépôt... donc le script `.gitlab-ci.yml` a été lancé.

On parle de `pipeline`...

---

# Exécution du fichier `.gitlab-ci.yml` 1/3

* Cliquer sur le menu `CI / CD`
* (Cliquer éventuellement sur le sous-menu `Pipelines`)

.center[![Framagit — Pipeline en cours d'exécution](images/framagit_running_pipeline.png)]

---

# Exécution du fichier `.gitlab-ci.yml` 2/3

* Cliquer sur le bouton `Running` ou `Passed` ou `Failed`
* Cliquer sur le bouton du job `generate_ssh_key`, pour voir la tâche en train de s'exécuter :

.center[![Framagit — Pipeline en cours d'exécution](images/framagit_running_job.png)]

---

# Exécution du fichier `.gitlab-ci.yml` 3/3

Si une erreur se produit (_pipeline failed_) :
* Cliquer sur le menu `Repository`
* (Cliquer éventuellement sur le sous-menu `Files`)
* Cliquer sur le fichier `.gitlab-ci.yml`
* Cliquer sur le bouton `Edit`
* Corriger l'erreur

---

# Récupération de la clé publique SSH

Une fois l'exécution de la pipeline terminée avec succès :
* Copier la clé publique SSH `bot_id_rsa.pub` :
  * Sélectionner le bloc commençant par `ssh-rsa AAAA...` et se terminant par `...== bot`
  * Le copier
* Aller dans le projet `irfm-data`
* Cliquer sur le menu `Settings` / `Repository` en bas à gauche :

.center[![FramaGit — Menu Settings / Repository](images/framagit_settings_repository_menu.png)]

---

# Ajout de la clé publique dans les clés de déploiement

Dans la section `Deploy Keys`, cliquer sur le bouton `Expand`, puis :

* Donner un titre : `irfm-paula.forteza-bot`
* Coller la clé publique TODO : où la récupérer ?
* Cocher la case `Write access allowed`
* Cliquer sur le bouton `Add key`

.center[![FramaGit — Formulaire d'ajout d'une clé de déploiement](images/framagit_add_deploy_key.png)]

---

# Vérification de la présence de la clé publique dans les clés de déploiement

Dans la section `Deploy Keys`, cliquer sur le bouton `Expand`.

Vérifier la présence de la clé publique dans les clés de déploiement du projet :

.center[![FramaGit — Liste des clés de déploiement du projet](images/framagit_project_deploy_keys.png)]

---

# Retour au _pipeline_ du script de génération de la clé SSH

* Cliquer sur le menu `Projects` en haut à gauche
* Choisir le projet `irfm-prenom.nom-script`
* Une fois dans le projet, cliquer sur le menu `CI / CD`
* Revenir sur la console d'exécution du job `generate_ssh_key`

---

# Récupération de la clé privée SSH

* Copier la clé privée SSH `bot_id_rsa` :
  * Sélectionner le gros bloc commençant par :
     * `-----BEGIN RSA PRIVATE KEY-----`
    * et se terminant par :
      * `-----END RSA PRIVATE KEY-----`
  * Le copier
* Cliquer sur le menu `Settings` en bas à gauche
* Cliquer sur le sous-menu `CI / CD`

---

# Ajout de la clé privée dans les variables secrètes

Dans la section `Secret variables`, cliquer sur le bouton `Expand`, puis :

* Donner un nom de variable : `SSH_PRIVATE_KEY`
* Coller la clé privée TODO : où la récupérer ?
* **Ne pas cocher** la case `Protected`
* Cliquer sur le bouton `Add new variable`

.center[![FramaGit — Formulaire d'ajout d'une variable secrète](images/framagit_add_secret_variable.png)]

---

# Vérification de la présence de la clé privée dans les variables secrètes

Dans la section `Secret variables`, cliquer sur le bouton `Expand`.

Vérifier la présence de la clé privée dans la liste des variables secrètes du projet :

.center[![FramaGit — Liste des variables secrètes du projet](images/framagit_project_secret_variables.png)]

---

# Recherche du module bancaire 1/4

* Cliquer sur le menu `Repository` en haut à gauche
* Cliquer sur le nom du fichier `.gitlab-ci.yml`
* Cliquer sur le bouton  `Edit` dans la barre de droite
* Supprimer le bloc de 4 lignes : `generate_ssh_key...`
* Ajouter les blocs `before_script` et `fetch` ci-dessous, pour obtenir :
  ```yaml
  image: python:3

  before_script:
    - pip install --no-cache-dir git+https://git.weboob.org/weboob/stable.git

  fetch:
    script:
      - weboob-config modules
  ```
* Dans le champ `Commit message`, mettre quelque chose comme : `Remplacement de la tâche de génération de la clé SSH par l'affichage de la liste des modules Weboob.`
* Laisser le champ `Target Branch` à `Master`
* Cliquer sur le bouton `Commit changes`

---

# Recherche du module bancaire 2/4

Vérifier que le fichier de configuration GitLab CI est valide :

.center[![Framagit — Fichier de configuration GitLab CI valide](images/framagit_valid_gitlab_ci.png)]

---

# Recherche du module bancaire 3/4

* Cliquer sur le menu `CI / CD`
* (Cliquer éventuellement sur le sous-menu `Pipelines`)
* Cliquer sur le bouton `Running` ou `Passed` ou `Failed` du premier des `pipelines`
* Cliquer sur le bouton du job `fetch`, pour voir la tâche en train de s'exécuter (ou déjà exécutée ) :

.center[![Framagit — Pipeline en cours d'exécution](images/framagit_running_job_fetch.png)]

---

# Recherche du module bancaire 4/4

* Rechercher dans la console le nom du module bancaire correspondant à votre banque.
* Par exemple, si votre banque est la Banque Postale, le nom du module est `bp`.

.center[![Framagit — Pipeline avec le module de la banque postale affiché](images/framagit_running_job_fetch_bp.png)]

---

# Paramétrage du compte bancaire 1/4

* Éditer à nouveau le fichier `.gitlab-ci.yml`
* Ajouter dans le `before_script` ci-dessous une ligne pour installer la bibliothèque `unidecode`
* Modifier la ligne du bloc `fetch` ci-dessous (en remplaçant bp par le nom du module de votre banque) :
  ```yaml
  image: python:3

  before_script:
    - pip install --no-cache-dir unidecode
    - pip install --no-cache-dir git+https://git.weboob.org/weboob/stable.git

  fetch:
    script:
      - weboob-config add bp
  ```
* Dans le champ `Commit message`, mettre quelque chose comme : `Ajout du module bancaire.`
* Laisser le champ `Target Branch` à `Master`
* Cliquer sur le bouton `Commit changes`

---

# Paramétrage du compte bancaire 2/4

* Regarder l'exécution de la tâche
* Dans le cas de _bp_ le script demande le paramètre `login` :

.center[![Framagit — Pipeline avec erreur de login](images/framagit_running_job_fetch_login.png)]

* Ajouter le `login` avec votre numéro de compte à la ligne et recommencer :
  ```bash
  weboob-config add bp login=VOTRE_NUMERO_DE_COMPTE
  ```

---

# Paramétrage du compte bancaire 3/4

* La console montre qu'il manque maintenant le mot de passe :

.center[![Framagit — Pipeline avec erreur de mot de passe](images/framagit_running_job_fetch_password.png)]

* Ajouter maintenant le `password` :
  ```bash
  weboob-config add bp login=NUMERO_COMPTE password=MOT_DE_PASSE
  ```

---

# Paramétrage du compte bancaire 4/4

*  Continuer jusqu'à ce que le script s'exécute sans erreur :
  ```bash
  weboob-config add bp login=NUMERO_COMPTE password=MOT_DE_PASSE website=par
  ```

.center[![Framagit — Pipeline d'ajout complet du compte banque postale](images/framagit_running_job_backend_added.png)]

---

# Masquage des paramètres cachés

* Aller dans `Settings` / `CI / CD` / `Secret variables`
* Ajouter les variables secrètes `USERNAME` et `PASSWORD` avec respectivement le numéro et le mot de passe du compte bancaire
* Modifier la ligne ci-dessous pour utiliser ces 2 variables
  ```bash
  weboob-config add bp login=${USERNAME} password=${PASSWORD} website=par
  ```
* Vérifier que cela fonctionne toujours.

---

# Remplacement des paramètres par un fichier de configuration

À la fin du `before_script`, ajouter les lignes :
  ```yaml
  - mkdir -p ~/.config/weboob
  - echo "[bp]" >> ~/.config/weboob/backends
  - echo "_module = bp" >> ~/.config/weboob/backends
  - echo "website = par" >> ~/.config/weboob/backends
  - echo "login = ${USERNAME}" >> ~/.config/weboob/backends
  - echo "password = ${PASSWORD}" >> ~/.config/weboob/backends
  - echo "" >> ~/.config/weboob/backends
  - chmod go-rwx ~/.config/weboob/backends
  ```

Cela crée un répertoire `~/.config/weboob`, ajoute dedans un fichier `backends` non lisible par d'autres, contenant :
```
[bp]
_module = bp
website = par
login = NUMERO_COMPTE
password = MOT_DE_PASSE
```

---

# Récupération de l'identifiant interne du compte créé 1/2

* Ajouter dans le `before_script` la ligne :
  ```bash
  - pip install --no-cache-dir pdfminer.six
  ```
* Remplacer dans le `before_script` la ligne :
  ```bash
  - pip install --no-cache-dir git+https://git.weboob.org/weboob/stable.git
  ```
  par
  ```bash
  - pip install --no-cache-dir git+https://git.weboob.org/weboob/devel.git
  ```
* Remplacer la ligne de la tâche `fetch` par :
  ```yaml
  - boobank -f account_list list
  ```

---

# Récupération de l'identifiant interne du compte créé 2/2

Après exécution, retrouver dans la console d'intégration continue quelque chose comme :
```
                                Account                     Balance    Coming
---------------------------------------------------------+----------+----------
                 IDENTIFIANT@bp COMPTE BANCAIRE             7741.26
---------------------------------------------------------+----------+----------
                                             Total (EUR)    7741.26       0.00
```

---

# Récupération des écritures depuis juillet 1/2

Remplacer la ligne de la tâche `fetch` par :
```yaml
- boobank history IDENTIFIANT@bp 2017-07-01
```

Pour obtenir quelque chose comme :
```
 Date         Category     Label                                                  Amount
------------+------------+---------------------------------------------------+-----------
 2017-07-30                FRAIS PAIEMENT CARTE INTERNATIO.                       -31.01
 2017-07-30                OWEN RECOLETA ARGENTIN 1140,00 ARS 1ARS= 0,048903E     -55.75
 2017-07-30                FRAIS PAIEMENT CARTE INTERNATIO.                        -1.28
 2017-07-30                OWN RECOLETA ARGENTIN 21947,40 ARS 1ARS= 0,048907E   -1073.39
 2017-07-30                FRAIS PAIEMENT CARTE INTERNATIO.                       -24.69
 2017-07-30                TRAITEUR ITALIE                                        -60.00
 2017-07-30                LATAM.COM FRANC 00000000 2687,26 687                 -2687.26
 2017-07-20                COMMISSION RETRAIT PAR CARTE                            -4.54
 2017-07-20   RETRAIT      RETRAIT AV. DEL LIBERTA 17.11.17 ARS 1106,20 CARTE     -53.82
 2017-07-13   RETRAIT DAB  CARTE X0834 10/11/17 A 12H12 RETRAIT DAB ASSEMBLEE     -20.00
 2017-07-13   RETRAIT DAB  CARTE X5180 10/11/17 A 12H11 RETRAIT DAB ASSEMBLEE     -20.00
 2017-07-07                AVANTAGE SUR COTISATION FORMULE DE COMPTE               36.93
 2017-07-07   Bank         COTISATION TRIMESTRIELLE DE VOTRE FORMULE DE COMPT     -36.93
```

---

# Récupération des écritures depuis juillet 2/2

* Ajouter l'option `-n 100` pour demander à récupérer plus de lignes (100)
* Ajouter l'option `-f csv` pour afficher les lignes au format CSV
* C'est à dire :
  ```yaml
  - boobank history IDENTIFIANT@bp 2017-07-01 -f csv -n 100
  ```

Pour obtenir quelque chose comme :
```
id;url;date;rdate;vdate;type;raw;category;label;amount;card;commission;original_amount;original_currency;country;investments
@bp;Not loaded;2017-11-30;2017-11-30;Not loaded;0;FRAIS PAIEMENT CARTE INTERNATIO.;Not available;FRAIS PAIEMENT CARTE INTERNATIO.;-31.01;Not loaded;Not loaded;Not loaded;Not loaded;Not loaded;[]
@bp;Not loaded;2017-11-30;2017-11-30;Not loaded;11;DEBIT CARTE BANCAIRE DIFFERE;Not available;DEBIT CARTE BANCAIRE DIFFERE;-4129.37;Not loaded;Not loaded;Not loaded;Not loaded;Not loaded;[]
@bp;Not loaded;2017-11-30;2017-11-20;Not available;12;FLORERIA ATLANT ARGENTIN 1468,00 ARS 1ARS= 0,048910EUR;Not available;FLORERIA ATLANT ARGENTIN 1468,00 ARS 1ARS= 0,048910EUR;-71.80;Not loaded;Not loaded;Not loaded;Not loaded;Not loaded;[]
@bp;Not loaded;2017-11-30;2017-11-20;Not available;12;FRAIS PAIEMENT CARTE INTERNATIO.;Not available;FRAIS PAIEMENT CARTE INTERNATIO.;-1.65;Not loaded;Not loaded;Not loaded;Not loaded;Not loaded;[]
@bp;Not loaded;2017-11-30;2017-11-19;Not available;12;CRONICO BAR ARGENTIN 2128,00 ARS 1ARS= 0,048909EUR;Not available;CRONICO BAR ARGENTIN 2128,00 ARS 1ARS= 0,048909EUR;-104.08;Not loaded;Not loaded;Not loaded;Not loaded;Not loaded;[]
```

---

# Ajout des écritures dans le projet des relevés bancaires 1/6

Aller sur la page `Overview` de votre projet `irfm-data` et copier l'URL `SSH` du projet, de la forme `git@framagit.org:VOTRE_COMPTE/irfm-data.git`.

Puis retourner sur le script et remplacer le `script` de la tâche `fetch` par :
```yaml
before_script:
  - eval $(ssh-agent -s)
  - ssh-add <(echo "$SSH_PRIVATE_KEY")
  - mkdir -p ~/.ssh
  - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

  - pip install --no-cache-dir pdfminer.six
  - pip install --no-cache-dir unidecode
  - pip install --no-cache-dir git+https://git.weboob.org/weboob/devel.git

  - mkdir -p ~/.config/weboob
  - echo "[bp]" >> ~/.config/weboob/backends
  - echo "_module = bp" >> ~/.config/weboob/backends
  - echo "website = par" >> ~/.config/weboob/backends
  - echo "login = ${USERNAME}" >> ~/.config/weboob/backends
  - echo "password = ${PASSWORD}" >> ~/.config/weboob/backends
  - echo "" >> ~/.config/weboob/backends
  - chmod go-rwx ~/.config/weboob/backends
```

---

# Ajout des écritures dans le projet des relevés bancaires 2/6

```yaml
fetch:
  script:
    - git clone git@framagit.org:VOTRE_COMPTE/irfm-data.git
    - cd irfm-data/
    - boobank history IDENTIFIANT@bp 2017-07-01 -f csv -n 100 > releve-2017-08-01.csv
    - git add releve-2017-08-01.csv
    - git commit -m "Récupération du dernier fichier CSV" || true
    - git push --set-upstream origin master || true
```

---

# Ajout des écritures dans le projet des relevés bancaires 3/6

La tâche pécédente effectue les opérations suivantes :
* Récupérer le projet `irfm-data` des relevés bancaires (`git clone...`)
* Aller dans ce projet (`cd irfm-data/`)
* Récupérer les écritures depuis le 1er juillet et les mettre dans un fichier daté du 1er août
* Ajouter ce fichier dans la liste des fichiers à ajouter au projet `irfm-data` (`git add...`)
* Effectuer les modifications du projet `irfm-data` (`git commit...`)
* Mettre à jour le projet `irfm-data` avec ces modifications (`git push`)

Les 4 lignes ajout&es au début du `before_script` utilisent la variable secrète `SSH_PRIVATE_KEY` créée au début de l'atelier pour donner accès en écriture au projet `irfm-data`.

Aller regarder l'exécution de la tâche puis le projet `irfm-data` pour vérifier que le fichier `releve-2017-08-01.csv` apparaît au bout de quelques minutes.

---

# Ajout des écritures dans le projet des relevés bancaires 4/6

Modifier le script pour faire la même chose pour les mois suivants jusqu'à Octobre :
```yaml
    - [...]
    - boobank history IDENTIFIANT@bp 2017-08-01 -f csv -n 100 > releve-2017-09-01.csv
    - git add releve-2017-09-01.csv
    - [...]
```

...

```yaml
    - [...]
    - boobank history IDENTIFIANT@bp 2017-10-01 -f csv -n 100 > releve-2017-11-01.csv
    - git add releve-2017-11-01.csv
    - [...]
```

---

# Ajout des écritures dans le projet des relevés bancaires 5/6

Sachant que :
* La commande `date --iso-8601` retourne la date du jour au format AAAA-MM-JJ.
* La commande `date +%Y-%m-01` retourne le premier jour du mois au format AAAA-MM-JJ.
* La commande `date --iso-8601 --date="$(date +%Y-%m-01) -1 month"`retourne le premier jour du mois **précédent** au format AAAA-MM-JJ.

---

# Ajout des écritures dans le projet des relevés bancaires 6/6

Modifier le `script`, pour qu'il récupère le relevé du mois précédent et le mette dans un fichier daté du jour :
```yaml
fetch:
  script:
    - git clone git@framagit.org:VOTRE_COMPTE/irfm-data.git
    - cd irfm-data/
    - export CSV_FILENAME="releve-`date --iso-8601`.csv"
    - boobank history IDENTIFIANT@bp `date --iso-8601 --date="$(date +%Y-%m-01) -1 month"` -f csv -n 500 >${CSV_FILENAME}
    - git add ${CSV_FILENAME}
    - git commit -m "Récupération du dernier fichier CSV" || true
    - git push --set-upstream origin master || true
```

Enregistrer les modifications et vérifier qu'un fichier daté du jour apparaît bien et que ses écritures commencent bien le 1er jour du mois précédent.

---

# Récupération des données dans un tableur

* Aller sur un fichier CSV
* Cliquer sur l'icône _Open raw_ en haut à droite, à côté du bouton `Edit`
* Cliquer avec le bouton droit et sélectionner un menu _Enregistrer la page sous..._
* Ouvrir le fichier enregistré avec un tableur

---

# Suppression des variables secrètes

* Aller dans `Settings` / `CI / CD` / `Secret variables` et supprimer les variables secrètes `USERNAME` et `PASSWORD`.
* Retourner éditer le fichier `.gitlab-ci.yml`, pour ajouter les lignes suivantes au début du `script` de `fetch` :
  ```yaml
  - if [ -z "${USERNAME}" ]; then exit 1; fi
  - if [ -z "${PASSWORD}" ]; then exit 2; fi
  ```

Ces 2 lignes empêchent le script de s'exécuter si une des deux variables manquent.

---

# Mise à jour manuelle des relévés bancaires

Dans le cadre du [bureau ouvert de Paula Forteza](https://forteza.fr/index.php/2017/10/26/bureau-ouvert/), nous avons développé une petite interface utilisateur : https://irfm.parlement-ouvert.fr/

.center[![IRFM — Page de saisie du compte](images/irfm_ui_main.png)]

Vérifier qu'en saisissant vos informations, la tâche s'exécute bien...

---

# Git

* Utilisé quasiment par tous les développeurs
* Permet de voir l'historique du code
  * Aller dans le menu `Repository` / `Commits`
  * Cliquer sur un numéro de commit pour voir les modifications.
* Permet de voir l'historique des relevés bancaires
* Pourrait aussi servir pour la loi (historique, amendements)

---

# Lab : Publier ses frais de mandat en open data

## Atelier du 12 décembre 2017

.center[<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>]

Cette présentation est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.

Elle est disponible sur FramaGit :
.center[https://framagit.org/paula.forteza/ateliers-lab.git]

Pour voir cette présentation sur votre ordinateur :
```bash
git clone https://framagit.org/paula.forteza/ateliers-lab.git
cd ateliers-lab/2017-12-12_atelier_irfm_open_data/
python3 -m http.server
```

© 2017 Paula Forteza et son équipe
