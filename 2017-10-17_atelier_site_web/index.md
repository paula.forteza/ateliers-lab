class: center, middle

# Lab : Choisir et installer un site web

## Atelier du 17 octobre 2017


_La présentation de cet atelier est disponible en ligne sur :_
.center[http://paula.forteza.frama.io/ateliers-lab/]

---

# Présentation du ![Logo du Lab](images/logo_lab.jpeg) La REM

## Pourquoi cet atelier ?

---

# Objectifs

* Que chacun reparte avec son propre site web
* en sachant comment il fonctionne
* en étant conscient des enjeux

[![Thème du site d'exemple](images/site_frédéric_petit.png)](http://frederic-petit.eu/)

---

# Déroulé de l'atelier

1. Les différentes solutions de sites web
2. Les critères de choix
3. La solution retenue
4. Installation et configuration du site web

---
class: center, middle

# 1. Les différentes solutions de sites web

---

# Faut-il un site web ?

* Ne pas croire que parce que vous avez un site web les citoyens viendront dessus.
  * Seul un sous-ensemble de certaines catégories de citoyens viendront régulièrement
* Il faut aller où sont les citoyens plutôt que de tenter de les faire venir.
* Pour chaque information publiée (article, photo, video, stream) **se demander où publier pour avoir le plus d'audience**.
* Mais des services privés comme Facebook ne peuvent pas suffire.
  * Une proportion non négligeable de personnes y sont réfractaires et pour de bonnes raisons.
  * Sans compte Facebook, LinkedIn, etc, il n'est pas possible de consulter les pages.

---

# Il faut un site web

* Portail recensant ce qui est publié ailleurs.
* Lien unique donné dans toutes les actions de communication (en plus des réseaux sociaux).

Le but de cet atelier est de vous proposer une plateforme :
* qui répond à ce besoin
* sans en faire trop
* pour un faible investissement :
  * en argent
  * et en temps

---

# Différentes solutions de sites web

Plusieurs possibilités pour réaliser un site web :

* Développer un site web sur mesure de A à Z
* Utiliser une solution existante répondant à l'ensemble de vos besoins (application intégrée)
* Utiliser différents logiciels et services répondant chacun à un besoin précis
* Mixer ces 3 solutions

---

# Développer un site web sur mesure

.center[![Panneau danger](images/panneau_danger.png)]

Un développement informatique est toujours plus compliqué, plus lent que prévu.

Le résultat d'un développement est toujours décevant par rapport à ce qui était imaginé.

La plupart des développements échouent.

Ne vous lancez pas dans un développement spécifique, sauf si c'est l'élément clé de votre mandat... Mais dans ce cas, vous n'assisteriez sans doute pas à cet atelier.

---

# Cas particulier des « petits génies » en informatique

Si vous avez dans votre équipe un « petit génie » en informatique, qui vous incite à développer sur mesure votre propre site web :

* Aphorisme :
  > Il n'existe pas de bons développeurs, juste des informaticiens qui sont moins nuisibles que les autres.
* Par définition, un développeur aime développer. Il aime beaucoup moins réutiliser les logiciels développés par d'autres.
* Rappel : **La plupart des développements échouent ou s'avèrent décevants.**
* Développer est beaucoup plus facile et motivant que maintenir et faire évoluer.
* Le meilleur développement est celui qui est déjà fini, testé et utilisé par un grand nombre de personnes.

---

# Solution intégrée : Nation Builder

Tou.te.s les député.e.s présent.e.s ici connaissent le produit.

.center[![Logo de Nation Builder](images/logo_nation_builder.jpeg)]

Parmi ses inconvénients : c'est un service américain.

Il est sans doute trop tard pour convertir le site de campagne en site de député.e : le délai limite était le 30 septembre.

Pour se conformer à la législation, toutes les données personnelles fournies par _La REM_ ont été supprimées. Seules celles saisies vous-mêmes sont conservées.


---

# Solutions intégrées alternatives à Nation Builder

Autres solutions françaises :

* [Cinquante + un](http://www.liegeymullerpons.fr/fr/) de Liegey Muller Pons

.center[![Logo de cinquante + 1](images/cinquante_plus_1.png)]

* Solution libre de _La France insoumise_
  * S'est complètement détachée de _Nation Builder_ en Septembre :
    https://au43.fr/2017/09/29/plateforme-nouvelle-est-arrivee/
  * Plusieurs raisons de quitter _Nation Builder_ et de choisir du libre

---

# Solution intégrée légère : Eneko

.center[![Logo d'Eneko](images/logo_eneko.png)]

Service sur mesure, pensé pour les élus

Solution spécifique développée par un jeune d'_En Marche !_, réalisé sur mesure avec et pour un député

http://elus.eneko.co/

* Agenda
* Permanences
* Statistiques
* Blog
* Vidéos
* Articles de presse

---

# Solution intégrée légère : Simplébo

.center[![Logo de Simplébo](images/logo_simplébo.png)]

Offre proposée par une start-up française déjà en contact avec _En Marche !_

Un outil lui aussi très simple, mais adapté à plusieurs métiers.

Une accompagnement à distance par un conseiller pour aider à créer le site.

https://www.simplebo.fr/

---

# Alternative générique : le CMS

Un système de gestion de contenu (_ie content management system CMS_) est un logiciel spécialisé dans la publication d'articles.

Il existe des dizaines de CMS plus ou moins évolués permettant de réaliser des blogs, des journaux et pouvant répondre aux besoins des député.e.s.

Les CMS sont généralement extensibles par des systèmes de greffons (_plugins_).

Les CMS répondent aux besoins de base d'un site pour députés : blog, agenda, photos et vidéos, etc.

Les CMS les plus utilisés comme SPIP, WordPress comptent des milliers voire des millions d'utilisateurs.

Les CMS sont le plus souvent des logiciels libres et gratuits. Les CMS propriétaires ont été pour la plupart marginalisés

---
class: center, middle

# 2. Les critères de choix

---

# Notre motivation

* Autonomie des députées
* Sans dépendre des prestataires
* À moindre coût
* En maîtrisant l'usage des logiciels et des données

---

# L'hébergement des données personnelles

## Directives européennes

* _Privacy Shield_
* _General Data Protection Regulation (GDPR)_

## En résumé

Il est souhaitable d'héberger son site en France ou en Europe.

Si on héberge soi-même pas de souci.

Sinon vérifier son prestataire.

Thématique à l'agenda des travaux parlementaires dans quelques semaines.

---

# Héberger en France

Il existe une multitude d'hébergeurs en France.

Online (groupe _Free_) est avec _OVH_ un des deux principaux hébergeurs de taille européenne sinon mondiale, que la France a la chance de compter.

## Disgression sur le cloud souverain

---

# Exemple de Framasoft et des chatons

Pour ceux qui ne veulent/savent pas héberger eux-mêmes, l'association Framasoft propose tout une gamme de services à base de logiciels libres et respectant tous les critères de protection des données personnelles.

.center[https://framasoft.org/]

Par exemple FramaSite. Mais :
> Framasite est en phase de test. Le service fonctionne, mais n’est pas encore facile à utiliser par quiconque.

* Moins de garantie de fonctionnement
* Mais plus de bienveillance de la part d'une communauté

---

# Avantages et inconvénients des différents solutions

La plupart des solutions intégrées sont des services propriétaires : pas de maîtrise de l'hébergement des données

Une solution intégrée cherche à répondre à tous les besoins des député.e.s. Elle est plus homogène à utiliser qu'un ensemble d'outils.

Les solutions intégrées complètes sont généralement très riches en fonctionnalités : elles cherchent à couvrir tous les besoins. Mais chaque député.e. n'a besoin que d'un sous-ensemble des fonctionnalités proposées.

Les solutions intégrées simples n'ont pas cet écueil, mais tôt ou tard il va manquer une fonctionnalité.

Les logiciels intégrés sont généralement plus difficilement interopérables avec d'autres logiciels.

Les logiciels génériques comme les CMS nécessitent plus de paramétrage et doivent souvent être étendus par d'autres logiciels.

---
class: center, middle

# 3. La solution retenue

---

# Solution retenue

Notre objectif est de vous montrer que vous pouvez être autonomes sur le numérique.

Nous avons donc retenu un CMS libre et un hébergeur.

* WordPress (car c'est le plus standard)

.center[![Logo de WordPress](images/logo_wordpress.png)]

* Chez un hébergeur français Online (car c'est un des plus gros en France)

.center[![Logo de WordPress](images/logo_online.png)]

On aurait pu choisir beaucoup d'autres solutions. Mais il fallait en choisir une pour l'atelier.

---
class: center, middle

# 4. Installation et configuration du site web

---

# Lancement de l'installation

Allez sur https://www.online.net/fr

Les différentes offres :
* serveurs dédiés : serveurs nus sur lesquels on doit tout installer soi-même
* stockage : service de stockage et de sauvegarde
* housing : serveurs qui leur sont apportés et qu'ils hébergent
* cloud : serveurs physiques ou virtuels lancés à la demande
* web : serveurs pré-installés avec un logiciel et notamment WordPress

---

# Hébergement web

Hébergement pro ou perso en fonction du nombre de mails

.center[![Offres d'hébergement web chez Online](images/offres_hebergement_web.png)]

---

# Choix d'un nom de domaine 1/2

.center[![Formulaire pour un nouveau nom de domaine](images/nouveau_nom_de_domaine.png)]

---

# Choix d'un nom de domaine 2/2

.center[![Formulaire pour un nouveau nom de domaine disponible](images/nouveau_nom_de_domaine_disponible.png)]

.center[![Bouton Commander](images/bouton_commander.png)]

---

# Choix d'un hébergement 1/2

.center[![Formulaire de choix d'un nouvel hébergement](images/nouvel_hébergement.png)]

---

# Choix d'un hébergement 2/2

Ne jamais faire l'impasse sur la sauvegarde.

Ne pas oublier de la tester rapidement... sinon on oublie avant qu'il ne soit trop tard.

.center[![Formulaire de choix d'une option de sauvegarde](images/option_sauvegarde.png)]

.center[![Bouton Commander](images/bouton_commander.png)]

---

# Création d'un compte 1/5

.center[![Formulaire de connexion ou inscription](images/connexion_inscription.png)]

Cliquer sur le bouton `Inscription`.

---

# Création d'un compte 2/5

.center[![Formulaire d'inscription](images/inscription.png)]

.center[![Formulaire d'inscription suite](images/inscription2.png)]

---

# Choisir un mot de passe

* Utiliser un mot de passe différent pour chaque site
* Utiliser une extension comme _Password Alert_ pour Chrome
  * afin de s'assurer que le mot de passe est unique pour chaque site
  * évite le phishing.
* Éviter d'utiliser un générateur de mots de passe en ligne.

---

# Création d'un compte 3/5

.center[![Formulaire de validation du compte](images/validation_compte.png)]

---

# Création d'un compte 4/5

Après clic sur le lien dans le courriel de confirmation :

Mettre un `-` dans le champ Société, puis cliquer sur le bouton `Créer le contact`.

.center[![Formulaire de création du contact](images/création_contact.png)]

---

# Création d'un compte 5/5

.center[![Panier](images/panier.png)]

---

# Paiement 1/2

.center[![Formulaire d'ajout d'une carte bancaire](images/ajout_carte_bancaire.png)]

---

# Paiement 2/2

.center[![Confirmation de commande](images/confirmation_commande.png)]

---

# Installation de WordPress 1/9

Attendre quelques instants, le temps de recevoir les 4 courriels de confirmation d'Online.

Cliquer sur le menu `Hébergement`.

.center[![Hébergement](images/hébergement.png)]

Cliquer sur le lien `Administrer`.

---

# Installation de WordPress 2/9

Cliquer sur le menu `Applications web` sur la gauche.

.center[![Applications web](images/applications_web_sans_base.png)]

Il faut au préalable configurer une base de données.

---

# Installation de WordPress 3/9

Cliquer sur le menu `Bases de données MySQL` sur la gauche.

.center[![Liste des bases de données](images/bases_de_données_mysql_vide.png)]

Cliquer sur le bouton `Ajouter une base de données`.

---

# Installation de WordPress 4/9


.center[![Formulaire de création de la base de données](images/création_base_de_données.png)]

---

# Installation de WordPress 5/9

La base de données est créée.

.center[![Liste des bases de données](images/bases_de_données_mysql.png)]

---

# Installation de WordPress 6/9

Cliquer à nouveau sur le menu `Applications web` sur la gauche.

.center[![Applications web](images/applications_web.png)]

---

# Installation de WordPress 7/9

.center[![Application WordPress](images/application_wordpress.png)]

---

# Installation de WordPress 8/9

.center[![Confirmation de l'installation](images/confirmation_installation.png)]

---

# Installation de WordPress 9/9

.center[![Installation en cours](images/installation_en_cours.png)]

Une fois l'installation terminée et après réception du courriel contenant le mot de passe, cliquer sur le lien `Administration`.

.center[![Installation terminée](images/installation_terminée.png)]

---

# Identification sur WordPress

Aller sur la page d'administation de votre site :
.center[http://_VOTRE_NOM_DE_DOMAINE_/wp-admin]

.center[![Identification](images/login.png)]

Saisissez l'identifiant et mot de passe reçu par courriel.

---

# Administration de WordPress


.center[![Administration de WordPress](images/admin.png)]

---

# Thèmes

WordPress comme la plupart des CMS propose différents thèmes par défaut.

Thème = apparence du site

Utilisez toujours un thème standard :
* Les logiciels évoluent en permanence.
  * Les thèmes doivent évoluer aussi.
  * Un thème spécifique freine la mise à jour et l'évolution

Si vous utilisez différentes applications :
* Ne pas chercher à unifier les interfaces
* Mettre des éléments graphiques communs (images, barre de menu)

---

# Greffons (_plugins_)

Un greffon (_plugin_) est un module permettant d'ajouter une fonctionnalité à Wordpress.

La plupart des CMS disposent d'un système de greffons.

Il existe 2 sortes de greffons :

* Les greffons de base développés par les développeurs officiels de Wordpress.
* Les greffons développés par des tierces parties

Avant d'utiliser un greffon non officiel, il faut s'assurer de sa pérennité, comme pour un logiciel.

---

# Mise à jour des logiciels

Il faut veiller à mettre à jour en permanence WordPress, ses thèmes et ses greffons

Les greffons sont une cause importante de failles de sécurités :

* Il sont souvent moins bien développés et maintenus que le cœur de WordPress.
* Pensez à les mettre à jour régulièrement.

---

# Mise à jour de WordPress 1/2

.center[![Mise à jour](images/mise_à_jour.png)]

---

# Mise à jour de WordPress 2/2

.center[![Mise à jour terminée](images/mise_à_jour_terminée.png)]

---

# Mise à jour des thèmes


* Cliquer sur le menu `Tableau de bord` / `Mises à jour` à gauche
* Sélectionner tous les thèmes à mettre à jour
* Cliquer sur le bouton `Mettre à jour les thèmes`

---

# Choix d'un thème 1/2

Cliquer sur le menu `Apparence` / `Thèmes` à gauche :

.center[![Thèmes](images/thèmes.png)]

---

# Choix d'un thème 2/2

Cliquer sur le thème _Twenty Seventeen_, puis sur le bouton `Activer` :

.center[![Thèmes](images/twenty_seventeen.png)]

---

# Prévisualisation du site

Pour voir l'apparence du site, cliquer sur le menu ![WordPress](images/menu_wordpress.png) / `Aller sur le site`, sur la barre du haut à gauche.

Pour revenir à l'administration Wordpress, cliquer sur le menu ![WordPress](images/menu_wordpress.png) / `Tableau de bord`, sur la barre du haut à gauche.

---

# Personnalisation de l'apparence

Cliquer sur le menu `Apparence` / `Personnaliser` à gauche.

Les modifications peuvent alors être effectuées en modifiant les champs de la barre de gauche ou directement en cliquant sur les boutons situés dans la partie centrale.

.center[![Thèmes](images/menu_personnalisation.png)]

---

# Création d'une page « À propos »

Cliquer sur le menu `Pages` / `Ajouter` à gauche.

.center[![Thèmes](images/page_à_propos.png)]

---

# Création d'une page sur les CGU

_Contrairement à un article de blog, une page est un élément statique du site, généralement visible depuis la barre des menus_

Cliquer sur le menu `Pages` / `Ajouter` à gauche et créer une nouvelle page décrivant les conditions générales d'utilisation (CGU) du site.

Il existe de nombreux modèles de CGU.

Pour trouver celle la mieux adaptée à votre site, faire une recherche sur `modèle CGU`.

---

# Modération des commentaires

Les articles de WordPress peuvent être commentés par des internautes.

Mais WordPress permet de gérer finement les commentaires.

Pour voir les réglages des commentaires, cliquer sur le menu `Réglages` / `Discussion` à  gauche

Laisser les réglages par défaut, au moins au début et regarder s'il vous conviennent à l'usage.

---

# Non déclaration du site à la CNIL

.center[https://www.cnil.fr/cnil-direct/question/393]

> Dans un but de simplification, la déclaration des sites web a été supprimée.
>
> Mais, si votre site web permet de collecter des données personnelles (questionnaire en ligne, commande en ligne, création d'un compte en ligne, etc.), vous devez alors déclarer le fichier que vous aurez constitué avec ces informations.

---

# Écrire vos premiers articles et faire connaître votre site

Notamment par la voie du mailing, mais ce sera le sujet d'un autre cours

Définir vos indicateurs de succès.

N'oubliez pas de mettre un lien vers votre site web partout. La popularité de votre site web augmente avec le nombre de liens externes qui y font référence.

---

# Lab : Choix et installation d'un site web

## Atelier du 17 octobre 2017

.center[<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>]

Cette présentation est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.

Elle est disponible sur FramaGit :
.center[https://framagit.org/paula.forteza/ateliers-lab.git]

Pour voir cette présentation sur votre ordinateur :
```bash
git clone https://framagit.org/paula.forteza/ateliers-lab.git
cd ateliers-lab/2017-10-17_atelier_site_web/
python3 -m http.server
```

© 2017 Paula Forteza et son équipe
