class: center, middle

# Lab : Choisir et installer une solution de communication massive

## Atelier du 28 novembre 2017


_La présentation de cet atelier est disponible en ligne sur :_
.center[http://paula.forteza.frama.io/ateliers-lab/]

---
class: center, middle

# Présentation du ![Logo du Lab](images/logo_lab.jpeg) La REM

## Pourquoi cet atelier ?

---

# Objectifs

* Que chacun reparte avec son propre service d'envoi de courriels
* en sachant comment il fonctionne
* en étant conscient des enjeux

.center[![Mailtrain — Page d'accueil](images/mailtrain_home.png)]

---

# Déroulé de l'atelier

1. Installation d'un serveur d'auto-hébergement
2. Pendant l'installation du service
  * Les différentes solutions d'envois de courriels
  * Les critères de choix
  * La solution retenue
3. Installation et configuration de la plateforme d'envoi de courriels
4. Envoi de courriels

---
class: center, middle

# 1. Installation d'un serveur d'auto-hébergement

---

# Location d'un serveur Scaleway

## https://cloud.scaleway.com/

.center[![Scaleway — Page d'accueil](images/scaleway_home.png)]

---

# Création d'un compte Scaleway

.center[![Scaleway — Formulaire de création d'un compte](images/scaleway_create_account_form.png)]

---

# Conditions générales d'utilisation

.center[![Scaleway — Conditions générales d'utilisation](images/scaleway_terms_of_service.png)]

---

# Envoi du courriel de validation

.center[![Scaleway — Envoi du courriel de validation](images/scaleway_validate_email.png)]

---

# Courriel de validation

.center[![Scaleway — Courriel de validation](images/scaleway_validation_email.png)]

---

# Saisie de l'adresse

.center[![Scaleway — Formulaire de saisie de l'adresse](images/scaleway_validate_address.png)]

---

# Ajout d'un mode de paiement

.center[![Scaleway — Formulaire d'ajout d'un mode de paiement](images/scaleway_add_payment_method.png)]

---

# Ajout d'une carte bancaire

.center[![Scaleway — Formulaire d'ajout d'une carte bancaire](images/scaleway_add_credit_card.png)]

---

# Traitement du paiement en cours

.center[![Scaleway — Carte enregistée avec succès](images/scaleway_card_successfully_registered.png)]

**Note** : Si on reste bloqué sur l'écran ci-dessous, il faut activer les _popups_ dans le navigateur.

.center[![Scaleway — Traitement du paiement en cours](images/scaleway_payment_processing.png)]

---

# Tableau de bord Scaleway

.center[![Scaleway — Tableau de bord](images/scaleway_dashboard.png)]

---

# Demande d'autorisation pour l'envoi de courriels

Dans l'onglet `Support`, cliquer sur le bouton `New Ticket` et remplir le formulaire :

* Subject: Ouverture port SMTP
* Message:
  > Je vous prie d'ouvrir le port SMTP en sortie.
  >
  > Vous trouverez ci-joint :
  > * Une numérisation de ma carte d'identité
  > * Un portrait où je tiens ma carte d'identité

N'oubliez pas de joindre les 2 photographies numérisées !

---

# Tableau de bord Scaleway

Retourner au tableau de bord en cliquant sur l'onglet `Dashboard`.

.center[![Scaleway — Tableau de bord](images/scaleway_dashboard.png)]

---

# Création d'un serveur

.center[![Scaleway — Bouton de création d'un serveur](images/scaleway_create_server_button.png)]

---

# Ajout de la clé publique SSH

.center[![Scaleway — Ajout d'une clé publique SSH](images/scaleway_add_ssh_key.png)]

La clé à ajouter :

> ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDS5zEwSVhXiyMJ+YRktLuA4yWUV55GuWo1B1KFJJjut4eJpUPhTIZ3qndaE4kYPooLSxpMj2cG2HzpioUbQ7R6fu7tw+4dR+6fuBuv0x8YYIaG6LT6qKUQbV2tPBp7GnUxIz8HKslZdA4Qj4FIulnpQ+1dOwaI/SGAkA9wLSY1aH70KEYNiawbPBPhk9sJ105yyqJY3dSLmzHV3XpRnIEfDW92psHNhwyPyyVeDo808RuyyIF52jAeJTfQXggBeT48hIPhL4iNijf3mXvbMeJLsbEa5gEv/GFxpcm2Z1yJ5CD35KIz0yff/nslaJ4cuEeTGiqU3ARi05ofeOQKc+7r keybox@global_key

---

# Ajout réussi de la clé publique SSH

.center[![Scaleway — Ajout réussi d'une clé publique SSH](images/scaleway_ssh_key_added.png)]

---

# Choix du serveur

.center[![Scaleway — Choix de la gamme de serveurs : Starter](images/scaleway_server_range_starter.png)]

.center[![Scaleway — Choix d'un serveur x86 VC1S](images/scaleway_x86_vc1s.png)]

---

# Choix du système d'exploitation

.center[![Scaleway — Choix d'une image](images/scaleway_choose_image.png)]

.center[![Scaleway — Barre de création d'un serveur](images/scaleway_create_server_bar.png)]

---

# Serveur démarré

.center[![Scaleway — Serveur Cloudron](images/scaleway_server.png)]

---

# Avant de se connecter au serveur

* Le serveur installé fonctionne uniquement sous forme de console texte.
* Pour y accéder il faut établir une connexion à distance entre la console (le terminal) de votre ordinateur personnel et le serveur.
* Cette connexion se fait avec le protocole _Secure Shell (SSH)_.
* Mais
  * Il faudrait installer un client SSH sur votre ordinateur.
  * Le wifi de l'Assemblée ne laisse pas passer le SSH.

Nous avons donc installé un client SSH web basé sur le logiciel libre [KeyBox](http://sshkeybox.com/).

---

# Serveur KeyBox

## https://keybox.parlement-ouvert.fr

1. Demander à l'équipe de formation de vous créer un compte KeyBox. Fournir l'adresse IP de votre serveur, votre nom et prénom, ainsi que ceux de votre député.
2. Aller sur https://keybox.parlement-ouvert.fr et s'identifier
  * Laisser le champ OTP Access Code vide

.center[![KeyBox — Identification](images/keybox_login.png)]

---

# Obtention d'un compte KeyBox

Écrire à `equipe@forteza.fr`, en précisant :
* Le prénom et nom de l'utilisateur
* Le prénom et nom du député
* L'adresse IP du serveur

---

# Authentification à double facteur sur KeyBox

.center[![KeyBox — Configuration des mots de passe à usage unique](images/keybox_otp_setup.png)]

Cliquer sur le bouton `Skip for Now`

---

# Qu'est ce que l'authentification à double facteur ? 1/2

Les moyens actuels d'authentifier un utilisateur :

* par quelque chose que l'utilisateur connaît (mot de passe, code à 4 chiffres, etc)
* par quelque chose que l'utilisateur détient (téléphone mobile, boitier spécifique)
* par quelque chose que l'utilisateur est (empreinte digitale, rétine, autre donnée biométrique)

---

# Qu'est ce que l'authentification à double facteur ? 2/2

L'authentification à double facteur est une combinaison de 2 des 3 moyens d'authentification.

=> Améliore le niveau de sécurité

Plusieurs solutions dont :
* FreeOTP
* Google Authenticator

---

# FreeOTP

.center[![FreeOTP — Copie d'écran](images/freeotp.png)]

---

# Présentation de SSH

SSH fonctionne avec 2 clés :
* 1 clé privée
  * protégée par un mot de passe
  * doit rester secrète
  * ne **jamais** la communiquer à un tiers
* 1 clé publique
  * à mettre sur chaque serveur où vous voulez vous connecter par SSH
  * peut être diffusée

---

# Mécanisme de clé privée et clé publique

Les fonctions à sens unique sont des fonctions mathématiques telles qu'une fois appliquées à un message, il est extrêmement difficile de retrouver le message original.

L'existence d'une brèche secrète permet cependant à la personne qui a conçu la fonction à sens unique de décoder facilement le message grâce à un élément d'information qu'elle possède, appelé clef privée.

Exemple :
* 2 nombres premiers = clé privée
* produit de ces 2 nombres = clé publique
  * = nombre quasi-premier
  * difficile de retrouver les 2 nombres premiers
    * quand les nombres premiers sont très grands

---

# Création de la clé SSH

.center[![KeyBox — Menu principal](images/keybox_main_menu_no_ssh.png)]

---

# Ajout d'une clé SSH

Cliquer sur le lien `Manage SSH Keys`.

.center[![KeyBox — Liste vide des clés SSH](images/keybox_manage_ssh_keys_empty.png)]

---

# Génération de la clé SSH

Cliquer sur le bouton `Add SSH Key`.

.center[![KeyBox — Génération de la clé SSH](images/keybox_generate_ssh_key.png)]

Choisir votre député comme profil.

Le mot de passe doit contenir des majuscules, des minuscules, des chiffres et des caractères spéciaux.

Enregistrer la clé secrète SSH sur votre ordinateur.

---

# Clé SSH créée

.center[![KeyBox — Liste des clés SSH contenant la clé créée](images/keybox_manage_ssh_keys_1_key.png)]

---

# Connexion au serveur par SSH 1/3

Cliquer sur le menu `Secure Shell / Composite SSH Terms` :

.center[![KeyBox — Menu Secure Shell](images/keybox_secure_shell_menu.png)]

---

# Connexion au serveur par SSH 2/3

.center[![KeyBox — Liste des terminaux SSH](images/keybox_composite_ssh_terminals.png)]

Sélectionner le terminal et cliquer sur le bouton `Create SSH Terminals` :

---

# Connexion au serveur par SSH 3/3

.center[![KeyBox — Terminal](images/keybox_terminal.png)]

---

# Installation de Cloudron sur le serveur

Cf https://cloudron.io/get.html

Télécharger le script d'installation de Cloudron :
```bash
wget https://cloudron.io/cloudron-setup
```

Rendre exécutable le script d'installation de Cloudron :
```bash
chmod +x ./cloudron-setup
```

Lancer le script d'installation de Cloudron :
```bash
./cloudron-setup --provider scaleway
```

Et patienter une trentaine de minutes...

---

# Achat d'un nom de domaine

Aller sur https://www.online.net/fr

Dans le menu `Ẁeb`, dans `Noms de domaine`, choisir `Commander`.

.center[![Online — Écran d'accueil](images/online_trouver_domaine.png)]

---

# Choix d'un nom de domaine

.center[![Formulaire pour un nouveau nom de domaine disponible](images/online_domaine_disponible.png)]

.center[![Bouton Commander](images/bouton_commander.png)]

---

# Nom de domaine sans hébergement

.center[![Formulaire de choix d'un nouvel hébergement](images/online_nouvel_hebergement.png)]


---

# Création d'un compte 1/5

.center[![Formulaire de connexion ou inscription](images/connexion_inscription.png)]

Cliquer sur le bouton `Inscription`.

---

# Création d'un compte 2/5

.center[![Formulaire d'inscription](images/inscription.png)]

.center[![Formulaire d'inscription suite](images/inscription2.png)]

---

# Choisir un mot de passe

* Utiliser un mot de passe différent pour chaque site
* Utiliser une extension comme _Password Alert_ pour Chrome
  * afin de s'assurer que le mot de passe est unique pour chaque site
  * évite le phishing.
* Éviter d'utiliser un générateur de mots de passe en ligne.

---

# Création d'un compte 3/5

.center[![Formulaire de validation du compte](images/validation_compte.png)]

---

# Création d'un compte 4/5

Après clic sur le lien dans le courriel de confirmation :

Mettre un `-` dans le champ Société, puis cliquer sur le bouton `Créer le contact`.

.center[![Formulaire de création du contact](images/création_contact.png)]

---

# Création d'un compte 5/5

Dans **Votre panier** :
* Décocher la case `Activer le renouvellement automatique`
* Cocher les 2 autres cases
* Cliquer sur le bouton `Valider ma commande`

---

# Paiement du nom de domaine

.center[![Formulaire d'ajout d'une carte bancaire](images/ajout_carte_bancaire.png)]

Une fois le paiement effectué, la page `Merci pour votre commande s'affiche`.

---

# Configuration du DNS pour y ajouter le serveur cloudron

* Attendre le courriel indiquant que le nom de domaine a été créé
* Aller sur https://console.online.net/
* Cliquer sur l'onglet _Domaine_.
* Cliquer sur le lien _Configurer le nom de domaine_.
* Cliquer sur l'onglet _Édition de la zone DNS_.
* Ajouter la ligne "my 86400 A pointe sur 163.172.167.164".
  * Remplacer 163... par l'adresse IP de votre ordinateur
* Ajouter la ligne "diffusion 86400 CNAME est un alias de my.paula-forteza.fr.".
  * Remplacer `paula-forteza.fr` par votre nom de domaine.
  * Le préfixe `my.` est nécessaire.
  * Un point final est nécessaire
* Cliquer sur le bouton _Enregistrer une nouvelle version_.
* Cliquer sur le bouton _Définir comme active_.

---

# Choix d'un service d'envoi de courriels

## MailChimp

La référence

## Mailwizz + Amazon SAS

Comme MailChimp mais on utilise Amazon Web Services pour envoyer les courriels => 10 fois moins cher

# Inconvénients de sous-traiter l'envoi de courriels

* vie privée : adresses courriels de personnes montrant un intérêt politique
* souveraineté : passer par les USA pour envoyer des courriels de France en France...

---

# À la recherche d'une solution libre

Cf [journal de cette recherche](https://framagit.org/paula.forteza/tutoriels/blob/master/envoi-en-masse-de-courriels.md)

# À la recherche d'une solution d'auto-hébergement

L'installation de Mailtrain sur un serveur nu était
* trop longue : > 2 heures
* trop complexe

=> Recherche d'une solution plus clé en main

---

# Cloudron

.center[![Cloudron — App Store](images/cloudron_app_store.png)]

---

# Mise à jour de Cloudron

Il **faut toujours** mettre à jour ses serveurs :
* pour des raisons de sécurité

Deux possibilités :
* Mises à jour manuelle
* Mises à jour automatique

---

# Mise à jour manuelle de Cloudron

* Avantage : c'et gratuit.
* Inconvénient : procédure complexe.
* cf  [_Manually updating your Cloudron Instance_](https://devfoundry.org/manually-updating-your-own-cloudron/)
* Documentation à venir :
  * plus détaillée,
  * en français
  * quand nous l'aurons testé plusieurs fois
  * restera complexe

---

# Mise à jour manuelle des applications de Cloudont

En plus de Cloudron, il faut mettre à jour chaque application installée.

## Mise à jour de Mailtrain

```bash
cloudron install --appstore-id org.mailtrain.cloudronapp
```

cf :
* https://git.cloudron.io/cloudron/box/wikis/UpdatingApps
* https://git.cloudron.io/cloudron/mailtrain-app

---

# Mise à jour automatique de Cloudron et des applications

* Nécessite de s'abonner.
* Mise à jour complètement automatique
* Ne remet pas en cause la protection des données personnelles :
  * C'est le serveur qui se met à jour automatiquement.
  * Ce n'est pas les équipes de Cloudron qui se connectent au serveur
* Permet de financer le projet libre Cloudron

---

# [Tarifs Cloudron](https://cloudron.io/pricing.html)

.center[![Cloudron — Tarifs](images/cloudron_pricing.png)]

---

# Obligations légales

* Obligations CNIL
  * [règles à respecter par les partis politiques, élus et candidats](https://www.cnil.fr/fr/elections/obligations-des-partis)
  * [Norme simplifiée NS-034 — Communication politique](https://www.cnil.fr/fr/declaration/ns-034-communication-politique)

La règlement général sur la protection des données RGPD arrive...

---

# [Norme simplifiée NS-034 — Communication politique](https://www.cnil.fr/fr/declaration/ns-034-communication-politique)

* Concerne :
  * les partis politiques, les élus ou les candidats
  * les fichiers de prospection ou de communication politiques
* Chaque courriel doit :
  * être envoyé uniquement à des personnes ayant consenti à être démarchées par voie électronique pour de la communication politique
  * indiquer comment ne plus recevoir de nouveaux messages
  * préciser l’origine des informations utilisées pour faire parvenir ce message

---

# Ouverture du port SMTP 1/2

.center[![Scaleway — Groupes de sécurité](images/scaleway_security_groups.png)]

Cliquer sur _Default security group_.

---

# Ouverture du port SMTP 2/2

.center[![Scaleway — Groupe de sécurité par défaut](images/scaleway_default_security_group.png)]

Mettre le bouton _Block SMTP_ sur _NO_.

---

# Rédémarrage du serveur Cloudron 1/2

Retourner sur la page du serveur dans le site Scaleway.

Cliquer sur le bouton `Off`.

.center[![Scaleway — Bouton off](images/scaleway_off_button.png)]

---

# Rédémarrage du serveur Cloudron 2/2

.center[![Scaleway — Redémarrage physique](images/scaleway_power_off_options.png)]

---

# Retour à l'installation de cloudron

Aller sur https://163.172.167.164/ (remplacer 163.172.167.164 par l'adresse IP de votre site)

Accepter le certificat.

---

# Configuration du nom de domaine cloudron

Fournir à Cloudron le nom de domaine configuré auparavant.

- Provide a domain for your Cloudron: paula-forteza.fr
- Choose how the domain is managed: Manual (not recommended)

---

# Welcome to your Cloudron

Créer son compte

---

# Réglage de l'envoi de courriels 1/2

* Aller dans le menu utilisateur en haut à droite / `Email`.

.center[![Cloudron — Activation des courriels](images/cloudron_enable_email.png)]

---

# Réglage de l'envoi de courriels 2/2

.center[![Cloudron — Confirmation de l'activation des courriels](images/cloudron_enable_email_dialog.png)]

---

# Réglage à effectuer dans le DNS

.center[![Cloudron — Champs DNS](images/cloudron_dns_records.png)]

---

# Réglage du MX

Cliquer sur `MX record` :

.center[![Cloudron — Champ MX](images/cloudron_mx_record.png)]

* Aller sur [console.online.net](https://console.online.net/), menu `Domaine`, action `Configurer le nom de domaine`.
* Cliquer sur l'onglet `Édition de la zone DNS`.
* Effacer toutes les lignes MX éventuellement existantes.
* Ajouter la ligne "@ 86400 MX est un alias de my.paula-forteza.fr. 10".
  * Remplacer `paula-forteza.fr` par votre nom de domaine.
  * Le préfixe `my.` est nécessaire.
  * Un point final est nécessaire

.center[![Online — Champ MX](images/online_mx_record.png)]

---

# Explication du MX

_Mail eXchanger_

.center[![Cloudron — Champ MX](images/cloudron_mx_record.png)]

Pour le domaine `paula-forteza.fr`, le serveur à qui adresser les courriels est `my.paula-forteza.fr`.

`10` est l'ordre des préférences des serveurs, quand il y a plusieurs lignes MS (le plus faible en premier).

---

# Réglage du DKIM

Cliquer sur `DKIM record` :

.center[![Cloudron — Champ DKIM](images/cloudron_dkim_record.png)]

Ajouter la ligne `cloudron._domainkey 86400 TXT "v=DKIM..."`.
  * Ne pas oublier les guillemets.

Effacer les autres lignes v=DKIM...

.center[![Online — Champ DKIM](images/online_dkim_record.png)]

---

# Explication du DKIM

_DomainKeys Identified Mail_

Vérification du nom de domaine par signature cryptographique du message

.center[![Cloudron — Champ DKIM](images/cloudron_dkim_record.png)]

* `v=DKIM1` version de DKIM
* `p=...` clé publique
* `t=s` signifie que ce domaine n'enverra pas de courriels utilisant un sous domaine

---

# Réglage du SPF

Cliquer sur `SPF record` :

.center[![Cloudron — Champ SPF](images/cloudron_spf_record.png)]

Ajouter la ligne `@ 86400 TXT "v=SPF..."`.
  * Ne pas oublier les guillemets.

Effacer les autres lignes v=SPF...

.center[![Online — Champ SPF](images/online_spf_record.png)]

---

# Explication du SPF

_Sender Policy Framework_

Vérification du nom de domaine de l'expéditeur d'un courriel

.center[![Cloudron — Champ SPF](images/cloudron_spf_record.png)]

* `v=spf1` version de SPF
* `a:my.paula-forteza.fr` vérifier que le serveur émetteur a bien un champ A dans le domaine my.paula-forteza.fr
* `include:spf.online.net` inclut la règle SPF du domaine spf.online.net
* `?all` neutre pour tous les autres émetteurs

---

# Description de `spf.online.net`

Utilisation du logiciel `dig` ou d'[une version en ligne](https://toolbox.googleapps.com/apps/dig/#TXT/)

.center[![Dig — Test du domaine spf.online.net](images/dig_txt_spf.online.net.png)]

---

# Réglage du DMARC

Cliquer sur `DMARC record` :

.center[![Cloudron — Champ DMARC](images/cloudron_dmarc_record.png)]

Ajouter la ligne `cloudron._domainkey 86400 TXT "v=DMARC..."`.
  * Ne pas oublier les guillemets.

Effacer les autres lignes v=DMARC...

.center[![Online — Champ DMARC](images/online_dmarc_record.png)]

---

# Explication du DMARC

_Domain-based Message Authentication, Reporting and Conformance_

Arbitrage entre SPF et DKIM

.center[![Cloudron — Champ DMARC](images/cloudron_dmarc_record.png)]

* `v=DMARC1` version de DMARC
* `p=reject` rejeter les messages non alignés
* `pct=100` pourcentage de messages sujets au rejet

---

# Mise à jour des serveurs DNS

Dans la console Online, activer la nouvelle configuration DNS :

* Cliquer sur le bouton _Enregistrer une nouvelle version_.
* Cliquer sur le bouton _Définir comme active_.

---

# Réglage du PTR

Cliquer sur `PTR record` :

.center[![Cloudron — Champ PTR](images/cloudron_ptr_record.png)]

* Aller dans le menu `Network` du site Scaleway.
* Cliquer sur l'adresse IP du serveur.
* Cliquer sur le champ "-" à côté du "REVERSE" et taper "my.paula-forteza.fr".
  * Ne **pas** mettre de point final
* Valider en cliquant sur la coche verte à droite.

.center[![Scaleway — Champ PTR](images/scaleway_reverse.png)]

---

# Explication du PTR

_PoinTeR_

.center[![Cloudron — Champ PTR](images/cloudron_ptr_record.png)]

Permet de connâitre le nom du serveur d'après son adresse IP.

---

# Vérification des réglages du DNS

Cliquer sur un des boutons de raffraichissemnt et vérifier que tout passe au vert.

.center[![Cloudron — Champs DNS](images/cloudron_dns_records_ok.png)]

---

# Vérification de l'état du SMTP

Cliquer sur un des boutons de raffraichissemnt et vérifier que tout passe au vert.

.center[![Cloudron — Statut SMTP](images/cloudron_smtp_status.png)]

---

# La boutique Cloudron

Cliquer dans le menu `App Store`.

.center[![Cloudron — App Store](images/cloudron_app_store.png)]

---

# Choix de Mailtrain dans l'App Store.

.center[![Cloudron — Présentation de Mailtrain](images/cloudron_mailtrain_features.png)]

Cliquer sur le bouton `Install`.

---

# Installation de Mailtrain dans l'App Store

* Location: diffusion  .paula-forteza.fr
* User management: Allow all users from this Cloudron

.center[![Cloudron — Installation de Mailtrain](images/cloudron_mailtrain_install.png)]

Cliquer sur le bouton `Install`.

---

# Personnalisation de Mailtrain

.center[![Cloudron — Personnalisation de Mailtrain](images/cloudron_mailtrain_customization.png)]

Cliquer sur le bouton `Got it`.

---

# Découverte de Mailtrain

Déplacer le curseur sur le bouton Mailtrain et noter l'apparition d'icônes sur la droite, pour supprimer, redémarrer ou configurer l'application.

Cliquer ensuite sur l'icône du train :

.center[![Cloudron — bouton Mailtrain](images/cloudron_mailtrain_button.png)]

---

# Site de Mailtrain

Noter le changement d'URL : on est passé de `my.paula-forteza.fr` à `diffusion.paula-forteza.fr`.

.center[![Mailtrain — Page d'accueil](images/mailtrain_home.png)]

---

# Création d'une liste de diffusion 1/4

Cliquer sur le menu `Lists` :

.center[![Mailtrain — Identification](images/mailtrain_login.png)]

---

# Création d'une liste de diffusion 2/4

.center[![Mailtrain — Listes](images/mailtrain_lists.png)]

Cliquer sur le bouton `Create List`.

---

# Création d'une liste de diffusion 3/4

.center[![Mailtrain — Création d'une liste](images/mailtrain_create_list.png)]

Cliquer sur le bouton `Create List`.

---

# Création d'une liste de diffusion 4/4

.center[![Mailtrain — Liste créée](images/mailtrain_list_created.png)]

Cliquer sur le bouton `List Actions` :

.center[![Mailtrain — Actions d'une liste](images/mailtrain_list_actions.png)]

---

# Ajout d'un abonné servant aux tests

.center[![Mailtrain — Nouvel abonné](images/mailtrain_new_subscriber.png)]

---

# Démonstration des modèles

.center[![Mailtrain — Modèles](images/mailtrain_templates.png)]

---

# Démonstration des campagnes

.center[![Mailtrain — Campagnes](images/mailtrain_campaigns.png)]

---
class: center, middle

# Exercices pratiques

---

# Arrêt définitif du serveur et désabonnement

Chez [Scaleway](https://cloud.scaleway.com/) :

* Cliquer sur le menu `Servers`.
* Cliquer sur le nom du serveur.
* Cliquer sur le bouton `OFF / ON`.
* Choisir l'option `Terminate`.
* Cliquer sur le bouton `Power off`.
* Cliquer sur le menu `Network`.
* Cliquer sur l'adresse IP.
* Cliquer sur le bouton `Delete`.

---

# Lab : Choisir et installer une solution de communication massive

## Atelier du 28 novembre 2017

.center[<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>]

Cette présentation est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.

Elle est disponible sur FramaGit :
.center[https://framagit.org/paula.forteza/ateliers-lab.git]

Pour voir cette présentation sur votre ordinateur :
```bash
git clone https://framagit.org/paula.forteza/ateliers-lab.git
cd ateliers-lab/2017-11-28_atelier_courriels/
python3 -m http.server
```

© 2017 Paula Forteza et son équipe
